const https = require('https');

exports.handler = function (event, context) {
    try{
        console.log("event.session.application.applicationID=" + event.session.application.applicationId);
    
        if(event.session.new) {
            onSessionStarted({requestId: event.request.requestId}, event.session);
        }
        
        switch(event.request.type) {
            case "LaunchRequest":
                onLaunch(
                    event.request, 
                    event.session,
                    (sessionAttr, speechletResponse) => {
                        context.succeed(buildResponse(sessionAttr, speechletResponse));
                    }
                );
                break;
            case "IntentRequest":
                onIntent(
                    event.request, 
                    event.session,
                    (sessionAttr, speechletResponse) => {
                        context.succeed(buildResponse(sessionAttr, speechletResponse));
                    }
                );
                break;
            case "SessionEndedRequest":
                onSessionEnded(event.request, event.session);
                context.succeed();
                break;
        }
    } catch (e) {
        context.fail("Exception: "+e);
    }
}

function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId="+sessionStartedRequest.requestId+", sessionId="+session.sessionId);
}

function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId="+launchRequest.requestId+", sessionId="+session.sessionId);
    
    var cardTitle = "GithubSearch",
        speechOutput = "Hello, you can search for projects or say exit to leave";
    
    callback(session.attributes, buildSpeechletResponse(cardTitle, speechOutput, "", false));
}

function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId="+intentRequest.requestId+", sessionId="+session.sessionId);

    var intent = intentRequest.intent;

    switch(intentRequest.intent.name) {
        case 'GithubSearch':
            handleProjectSearch(intent, session, callback);
            break;
        case 'AMAZON.HelpIntent':
            callback(session.attributes,
                buildSpeechletResponseWithoutCard(
                    "to use github search try, search for projects called hubot. To limit the results try, name the top ten projects called tetris. What would you like to do?",
                    "", 
                    "false")
                );
            break;
        case 'AMAZON.CancelIntent': 
            callback(session.attributes,buildSpeechletResponseWithoutCard("Canceling...","", "true"));
            break;
        case 'AMAZON.StopIntent': 
            callback(session.attributes,buildSpeechletResponseWithoutCard("Stopping...","", "true"));
            break;
        default:
            throw "invalid intent";
    }
}

function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId="+sessionEndedRequest.requestId+", sessionId="+session.sessionId);
}

function handleProjectSearch(intent, session, callback) {
    console.log("handleProjectSearch sessionId="+session.sessionId);
    var proj = null,
        limit = null;
    
    // Check to see if the project property is there
    // if it is get it.
    if( ! (intent.slots.Project.hasOwnProperty("value")) ) {
        callback(session.attributes,
                buildSpeechletResponseWithoutCard("Sorry, I didnt quite catch the project name","", "true")
                );
        return;
    }
    proj = intent.slots.Project.value;

    if(containsNonLatinCodepoints(proj)) {
        callback(session.attributes,
            buildSpeechletResponseWithoutCard("Sorry, It seems that searching for that project name isn't supported","", "true")
        );
        return;
    } 
    // Check to see if the limit property is there
    // if it is get it.
    if(intent.slots.Limit.hasOwnProperty("value")) {
        limit = intent.slots.Limit.value;
    }

    var config = {
        host: 'api.github.com',
        path: '/search/repositories?q='+proj,
        headers: {
            'User-Agent':"GithubSearchAlexaSkill",
            'Content-Type':"application/json"
        }
    }

    console.log(config.path);
    // Start an async request to fetch data from github
    https.get(config, (res) => {
        res.setEncoding('binary');
        const stat = res.statusCode;
        const contentType = res.headers['content-type'];
        
        let error;
        let raw = '';

        if(stat !== 200) {
            error = new Error('Request Failed. ' + `StatusCode: ${stat}`);
        } else if (!/^application\/json/.test(contentType)) {
            error = new Error('Invalid content-type \n' + `Expected application/json but recieved ${contentType}`);
        }
        if(error) {
            console.error(error.message);
            res.resume();
            callback(session.attributes,
                buildSpeechletResponseWithoutCard("Sorry, It looks like the search failed, please try again","", "true")
            );
            return;
        }

        res.on('data', (chunk) => {raw += chunk;});
        res.on('end', () => {
            try{
                const parsedData = JSON.parse(raw);
                callback(session.attributes, 
                        buildSpeechletResponseWithoutCard(
                          buildProjectText(limit,parsedData),
                          "", "true"
                          )
                        );
            } catch (e) {
                console.error(e.message);
            }
        });
    });
    
}

//-----------------------Helpers----------------------

function buildProjectText(limit, response) {
    if(limit === null || limit < 1) {
        limit = 20;
    }

    var items = response.items;
    var text = "Here are the projects I found. ";

    for(i = 0; i < limit; i++) {
        text += items[i].name + " by " + items[i].owner.login;
        if(i < limit-1) {
            text += ", ";
        }
    }

    return text;
}

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: title,
            content: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}


function buildSpeechletResponseWithoutCard(output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttr, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttr,
        response: speechletResponse
    }
}
function containsNonLatinCodepoints(s) {
        return /[^\x00-\x7F]/.test(s);
}
